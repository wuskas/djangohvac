#-*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User
from django.db.models import Q

from djangohvac.models import City, Country


class UserProfile(models.Model):
    class Meta:
        verbose_name_plural = u"Профиль"
        verbose_name = u"Профиль"

    user = models.OneToOneField(User, related_name="profile")


    firstName  = models.CharField(max_length=20)
    secondName = models.CharField(max_length=20)
    surName    = models.CharField(max_length=20)
    city       = models.ForeignKey(City,default=1)
    country    = models.ForeignKey(Country,default=1)
    avatar     = models.ImageField(upload_to='avatar', blank=True, default='static/img/users/user.jpg')
    aboutSelf  = models.TextField(max_length=1000)


    # def age(self):
    #     return relativedelta(timezone.now().date(), self.birthday).years

    def __unicode__(self):
        return self.user.username