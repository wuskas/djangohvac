#-*- coding: utf-8 -*-

import os, random, string
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
from django.shortcuts import render
from django.http import Http404, JsonResponse, HttpResponse, HttpResponsePermanentRedirect
from django.db.models import Q

from .models import UserProfile

def index(request):
    if not request.user.is_authenticated():
        return HttpResponsePermanentRedirect('/')
    else:
        return render(request, 'userprofile.jade')

def save_profile(request):
    if request.user.is_authenticated():
        profile = UserProfile.objects.filter(user_id = request.user.id).get()

    #change profile

    try:
        request.user.email = request.POST['email']
        request.user.save()
    except:
        return JsonResponse({'status':'login_fail', 'message':'Данные не сохранены'})


    profile.firstName   = request.POST['firstName']
    profile.secondName  = request.POST['secondName']
    profile.surName     = request.POST['surName']
    profile.city_id     = request.POST['city']
    profile.country_id  = request.POST['country']
    profile.aboutSelf   = request.POST['aboutSelf']
    profile.avatar      = request.POST['avatar']
    profile.save()

    return JsonResponse({'status':'success', 'message':'Настройки успешно сохранены'})

def uploadimg(request):
    tmp_name = ''.join(random.choice(string.letters + string.digits) for _ in range(10))
    tmp_name = ''.join(['avatar/', tmp_name, '.', request.FILES['avatar'].content_type.split('/')[1]])
    data = request.FILES['avatar']
    path = default_storage.save(tmp_name, ContentFile(data.read()))
    avatar = ''.join(['media/', path])

    return JsonResponse({'avatar' : avatar, 'status' : 'success'})