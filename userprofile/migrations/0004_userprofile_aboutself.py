# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0003_auto_20151109_1526'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='aboutSelf',
            field=models.TextField(default=b''),
        ),
    ]
