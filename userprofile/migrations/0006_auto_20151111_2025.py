# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0005_auto_20151111_1504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='aboutSelf',
            field=models.TextField(max_length=1000),
        ),
    ]
