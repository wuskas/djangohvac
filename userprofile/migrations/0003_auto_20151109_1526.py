# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0002_auto_20151106_1427'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='avatar',
            field=models.ImageField(upload_to=b'avatar', blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='surName',
            field=models.CharField(default=b' ', max_length=20),
        ),
    ]
