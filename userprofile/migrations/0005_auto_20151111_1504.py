# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0004_userprofile_aboutself'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='aboutSelf',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='surName',
            field=models.CharField(max_length=20),
        ),
    ]
