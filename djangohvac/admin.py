#-*- coding: utf-8 -*-

from django.contrib import admin
from .models import Country, City, Programm

admin.site.register(Country)

class CityAdmin(admin.ModelAdmin):
    list_display = ('title', 'country')
    list_filter = ['country']

admin.site.register(City, CityAdmin)

admin.site.register(Programm)
