# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('djangohvac', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='programm',
            name='url',
            field=models.CharField(default=b'www', max_length=20),
        ),
    ]
