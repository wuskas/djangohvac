#-*- coding: utf-8 -*-
from django.shortcuts import render, redirect, render_to_response
from django.http import JsonResponse, HttpResponse, HttpResponsePermanentRedirect
from django.template import RequestContext
from django.core.context_processors import csrf
from django.core import serializers

from .models import Country, City, Programm
from userprofile.models import UserProfile

#home page
def home(request):
    return render(request, 'index.jade')

#login in site page
def enter(request):
    if request.user.is_authenticated():
        return HttpResponsePermanentRedirect("/")
    return render(request,'login.jade')

#userprofile page
def user(request, user_id):
    if request.user.is_authenticated() and int(request.user.id) == int(user_id):
        return HttpResponsePermanentRedirect("/userprofile/")
    else:
        userpage = UserProfile.objects.filter(user_id = user_id).get()
        return render(request, 'user.jade', {'userpage' : userpage})

# def userslist(request):
#     users = UserProfile.objects.all()
#     users_countries = [item.country for item in UserProfile.objects.distinct('country').all()]
#     return render(request,'users.jade',{'users' : users, 'users_countries' : users_countries})

#registration page
def registration(request):
    if request.user.is_authenticated():
        return HttpResponsePermanentRedirect("/")
    return render(request, 'registration.jade')

#selection cities
def all_cities(request):
    country_id = int(request.POST['country'])
    cities     = City.objects.filter(country_id = country_id).order_by('title')
    message    = serializers.serialize("json", cities)
    return JsonResponse({'status' : 'success', 'message' : message})

#programm list 
def programm_list(request):
    programms = Programm.objects.all()
    return render(request,'programm.jade',{'programms' : programms})
