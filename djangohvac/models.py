#-*- coding: utf-8 -*-
import datetime
from django.utils import  timezone
from django.db import models
from django.contrib.auth.models import User


class Country(models.Model):
    class Meta:
        verbose_name_plural = u"Страна"
        verbose_name        = u"Страна"

    title = models.CharField(max_length=200)
    def __unicode__(self):
        return self.title

class City(models.Model):
    class Meta:
        verbose_name_plural = u"Город"
        verbose_name        = u"Город"

    title  = models.CharField(max_length=200)
    country = models.ForeignKey(Country)
    def __unicode__(self):
        return self.title

class Programm(models.Model):
    class Meta:
        verbose_name_plural = u"Программа"
        verbose_name        = u"Программа"
        
    title       = models.CharField(max_length=50)
    description = models.TextField(max_length=1000)
    img         = models.ImageField(blank=True)
    url         = models.CharField(max_length=20, default='www')
    def __unicode__(self):
        return self.title