#-*- coding: utf-8 -*-

from djangohvac.models import City, Country
from userprofile.models import UserProfile

def sitedata(request):
    users      = range(1,12)


    cities    = City.objects.all()
    countries = Country.objects.all()

    info = {          
        'users'      : users,
        'cities'     : cities,
        'countries'  : countries,
    }

    if request.user.is_authenticated():
        if not request.user.is_superuser:
            profile = UserProfile.objects.filter(user_id = request.user.id).get()
            info['profile'] = profile
            info['profile_cities'] = City.objects.filter(country_id = profile.country_id)

    return info