import requests
import hmac
import hashlib
import json, random, string

from requests_oauthlib import OAuth2Session

from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from django.conf import settings


from registration.users import UserModel
from registration.models import RegistrationProfile
from userprofile.models import UserProfile

from djangohvac.models import City, Country


def genAppSecretProof(app_secret, access_token):
    h = hmac.new (
        app_secret.encode('utf-8'),
        msg=access_token.encode('utf-8'),
        digestmod=hashlib.sha256
    )
    return h.hexdigest()


def login_in(request):

    username = request.POST['username']
    password = request.POST['password']

    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:
            login(request, user)
            return JsonResponse({'message':'success'})
        else:
            return JsonResponse({'message':'user_disabled'}, status=400)
    else:
        return JsonResponse({'message':'incorrect'}, status=400)

