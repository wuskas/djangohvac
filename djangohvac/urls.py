"""djangohvac URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView


from .register import MyRegistrationView
from . import login
from . import views


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^all-cities/$', views.all_cities, name='all_cities'),
    url(r'^$', views.home, name='home'),


    # #users
    # url(r'^users/$', views.userslist, name='users'),
    # url(r'^users/(?P<user_id>[0-9]+)/$', views.user, name='user'),

    #Enter
    url(r'^enter/', views.enter),

    #registration
    url(r'^accounts/activate/complete/', RedirectView.as_view(url=reverse_lazy('home'))),
    url(r'^accounts/login/', login.login_in),
    url(r'^accounts/register/', MyRegistrationView.as_view()),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^registration/$', views.registration),
    url(r'^accounts/logout/$','django.contrib.auth.views.logout', kwargs={'next_page':None}, name="logout"),

    #programm list
    url(r'^programm/list/$', views.programm_list),



    #profile settings
    url(r'^userprofile/', include('userprofile.urls')),
    url(r'^id/', include('diagramm.urls')),

]
