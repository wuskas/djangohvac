#-*- coding: utf-8 -*-
import json
from django.conf import settings
from django.http import HttpResponse
from django.views.generic.edit import CreateView
from django.template.loader import get_template
from django.shortcuts import render
from django.contrib.sites.models import Site
from django.contrib.auth import authenticate, login

from registration.users import UserModel
from registration.views import RegistrationView
from registration.models import RegistrationProfile
from registration import signals

from userprofile.models import UserProfile
from djangohvac.models import City, Country




class AjaxableResponseMixin(object):
    def get_form_kwargs(self):
        kwargs = {'initial': self.get_initial()}
        if self.request.method in ('POST', 'PUT'):
            dict_ = self.request.POST.copy()
            del dict_['csrfmiddlewaretoken']
            kwargs.update({
                'data': dict_,
                'files': self.request.FILES,
            })
        return kwargs

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)

        if self.request.is_ajax():
            return self.render_to_json_response(form.errors, status=400)
        else:
            return response

    def form_valid(self, request, form):
        if self.request.is_ajax():

            new_user = self.register(request, **form.cleaned_data)


            user_email = request.POST['email']
            success_message = get_template(
                'messages/register-success.jade').render({'user_email' : user_email})

            data = {
                'pk': new_user.pk,
                'message' : success_message
            }
            return self.render_to_json_response(data)
        else:
            response = super(AjaxableResponseMixin, self).form_valid(request, form)
            return response


class MyRegistrationView(AjaxableResponseMixin, RegistrationView):
    SEND_ACTIVATION_EMAIL = getattr(settings, 'SEND_ACTIVATION_EMAIL', True)

    def register(self, request, **cleaned_data):
        del cleaned_data['password2']
        username    = request.POST['username']
        email       = request.POST['email']
        password1   = request.POST['password1']


        new_user_instance = UserModel().objects.create_user(username, email, password1)



        firstName       = request.POST['firstName'].title()
        secondName      = request.POST['secondName'].title()

        
        new_userprofile_instance = UserProfile(
            user       = new_user_instance,
            firstName  = firstName,
            secondName = secondName,

        )

        new_userprofile_instance.save()
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)

        print site.domain
        site.domain = 'venta-tgz.ru'

        new_user = RegistrationProfile.objects.create_inactive_user(
            new_user=new_user_instance,
            site=site,
            send_email=self.SEND_ACTIVATION_EMAIL,
            request=request,
        )

        
        signals.user_registered.send(sender=self.__class__,
                                     user=new_user,
                                     request=request)

        # new_user = RegistrationProfile(user = new_user_instance, activation_key = 'ALREADY_ACTIVATED')
        # new_user.save()

        # new_user_instance.backend = 'django.contrib.auth.backends.ModelBackend'
        # login(request, new_user_instance)
        return new_user
