from django.contrib import admin
from .models import Calculation, DotCalculation, DotNotCalculation
# Register your models here.
admin.site.register(Calculation)
admin.site.register(DotCalculation)
admin.site.register(DotNotCalculation)