from django.db import models
from django.contrib.auth.models import User
from django.utils import  timezone

# Create your models here.
class Calculation(models.Model):
    master              = models.ForeignKey(User, related_name='master')
    title               = models.CharField(max_length=200,default = "default")
    created             = models.DateField(default = "2000-1-10")
    barometricpressure  = models.CharField(max_length=200,default = 99)

    def __unicode__(self):
        return self.title

class DotNotCalculation(models.Model):
    calculation         = models.ForeignKey(Calculation)
    temperature         = models.CharField(max_length=200)
    humidity            = models.CharField(max_length=200)
    enthalpy            = models.CharField(max_length=200)
    relativities        = models.CharField(max_length=200)
    selection_of_action = models.CharField(max_length=200)
    type_of_action      = models.CharField(max_length=200)
    heat_input          = models.CharField(max_length=200)
    moisture            = models.CharField(max_length=200)
    capacity            = models.CharField(max_length=200)
    air_mass_flow       = models.CharField(max_length=200)
    air_mass_flow2      = models.CharField(max_length=200)
    final_temperature   = models.CharField(max_length=200)
    final_humidity      = models.CharField(max_length=200)
    final_entalpy       = models.CharField(max_length=200)
    final_relativities  = models.CharField(max_length=200)
    from_process        = models.IntegerField(default=0)
    from_process2       = models.IntegerField(default=0)

class DotCalculation(models.Model):
    calculation            = models.ForeignKey(Calculation)
    temperature            = models.CharField(max_length=200)
    humidity               = models.CharField(max_length=200)
    entalpy                = models.CharField(max_length=200)
    relativities           = models.CharField(max_length=200)
    heat_input             = models.CharField(max_length=200)
    moisture               = models.CharField(max_length=200)
    capacity               = models.CharField(max_length=200)
    air_mass_flow          = models.CharField(max_length=200)
    air_mass_flow2         = models.CharField(max_length=200)
    final_temperature      = models.CharField(max_length=200)
    final_humidity         = models.CharField(max_length=200)
    final_entalpy          = models.CharField(max_length=200)
    final_relativities     = models.CharField(max_length=200)
    parc_pressure          = models.CharField(default=0, max_length=200)
    air_density            = models.CharField(default=0, max_length=200)
    dew_point              = models.CharField(default=0, max_length=200)
    saturation_temperature = models.CharField(default=0, max_length=200)