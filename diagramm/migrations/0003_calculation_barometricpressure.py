# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diagramm', '0002_auto_20160212_2132'),
    ]

    operations = [
        migrations.AddField(
            model_name='calculation',
            name='barometricpressure',
            field=models.CharField(default=99, max_length=200),
        ),
    ]
