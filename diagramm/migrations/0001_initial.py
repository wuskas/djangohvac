# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Calculation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('master', models.ForeignKey(related_name='master', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DotCalculation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('temperature', models.CharField(max_length=200)),
                ('humidity', models.CharField(max_length=200)),
                ('entalpy', models.CharField(max_length=200)),
                ('relativities', models.CharField(max_length=200)),
                ('heat_input', models.CharField(max_length=200)),
                ('moisture', models.CharField(max_length=200)),
                ('capacity', models.CharField(max_length=200)),
                ('air_mass_flow', models.CharField(max_length=200)),
                ('air_mass_flow2', models.CharField(max_length=200)),
                ('final_temperature', models.CharField(max_length=200)),
                ('final_humidity', models.CharField(max_length=200)),
                ('final_entalpy', models.CharField(max_length=200)),
                ('final_relativities', models.CharField(max_length=200)),
                ('parc_pressure', models.CharField(default=0, max_length=200)),
                ('air_density', models.CharField(default=0, max_length=200)),
                ('dew_point', models.CharField(default=0, max_length=200)),
                ('saturation_temperature', models.CharField(default=0, max_length=200)),
                ('calculation', models.ForeignKey(to='diagramm.Calculation')),
            ],
        ),
        migrations.CreateModel(
            name='DotNotCalculation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('temperature', models.CharField(max_length=200)),
                ('humidity', models.CharField(max_length=200)),
                ('enthalpy', models.CharField(max_length=200)),
                ('relativities', models.CharField(max_length=200)),
                ('selection_of_action', models.CharField(max_length=200)),
                ('type_of_action', models.CharField(max_length=200)),
                ('heat_input', models.CharField(max_length=200)),
                ('moisture', models.CharField(max_length=200)),
                ('capacity', models.CharField(max_length=200)),
                ('air_mass_flow', models.CharField(max_length=200)),
                ('air_mass_flow2', models.CharField(max_length=200)),
                ('final_temperature', models.CharField(max_length=200)),
                ('final_humidity', models.CharField(max_length=200)),
                ('final_entalpy', models.CharField(max_length=200)),
                ('final_relativities', models.CharField(max_length=200)),
                ('from_process', models.IntegerField(default=0)),
                ('from_process2', models.IntegerField(default=0)),
                ('calculation', models.ForeignKey(to='diagramm.Calculation')),
            ],
        ),
    ]
