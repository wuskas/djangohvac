# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diagramm', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='calculation',
            name='created',
            field=models.DateField(default=b'2000-1-10'),
        ),
        migrations.AddField(
            model_name='calculation',
            name='title',
            field=models.CharField(default=b'default', max_length=200),
        ),
    ]
