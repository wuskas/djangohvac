from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^new/$', views.new,   name='save_profile'),
    url(r'^calculate/$',views.calculate),
    url(r'^save/$',views.save_calculation),
    url(r'^old/$', views.old, name='calculation'),
    url(r'^old/(?P<calculation_id>[0-9]+)/$', views.old_calculation, name='old_calculation'),
]