#-*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import  HttpResponsePermanentRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from calculation import ProcessCalculation
from django.http import JsonResponse
import json 
import datetime
from django.utils import timezone

from .models import Calculation, DotCalculation, DotNotCalculation


def new(request):
    if request.user.is_authenticated():
        return render(request,'diagramm.jade')
    return HttpResponsePermanentRedirect("/enter/#login")

def calculate(request):
    i=1
    listvalue=[]
    counter=float(request.POST['counter'])
    while (i<=counter):
        selectionofaction=float(request.POST['selectionofaction_0'+str(i)])
        barometricpressure=request.POST['barometricpressure']
        if (selectionofaction==1):
            processes=float(request.POST['processes_0'+str(i)])
        elif(selectionofaction==0):
            dotvalue=float(request.POST['dotvalue_0'+str(i)])
        #Реализация процессов
        if (dotvalue==2 and selectionofaction==0):
            temperature = request.POST['temperature_0'+str(i)]
            enthalpy = 0
            humiditycontent = 0
            relativities = request.POST['relativities_0'+str(i)]
            parameters=ProcessCalculation(temperature,enthalpy,humiditycontent,relativities)
            parameters.find_valueair_with_temperature_and_relativities(parameters.temperature,parameters.relativities,barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'selectionofaction_0'+str(i):selectionofaction,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            }
        elif (dotvalue==0 and selectionofaction==0):
            temperature = request.POST['temperature_0'+str(i)]
            enthalpy = request.POST['enthalpy_0'+str(i)]
            humiditycontent = 0
            relativities = 0
            parameters=ProcessCalculation(temperature,enthalpy,humiditycontent,relativities)
            parameters.find_valueair_with_temperature_and_enthalpy(parameters.temperature,parameters.enthalpy,barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'selectionofaction_0'+str(i):selectionofaction,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            }
        elif (dotvalue==1 and selectionofaction==0):
            temperature = request.POST['temperature_0'+str(i)]
            enthalpy = 0
            humiditycontent = request.POST['humiditycontent_0'+str(i)]
            relativities = 0
            parameters=ProcessCalculation(temperature,enthalpy,humiditycontent,relativities)
            parameters.find_valueair_with_temperature_and_humiditycontent(parameters.temperature,parameters.humiditycontent,barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'selectionofaction_0'+str(i):selectionofaction,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            }
        elif (dotvalue==3 and selectionofaction==0):
            temperature = 0
            enthalpy = request.POST['enthalpy_0'+str(i)]
            humiditycontent = request.POST['humiditycontent_0'+str(i)]
            relativities = 0
            parameters=ProcessCalculation(temperature,enthalpy,humiditycontent,relativities)
            parameters.find_valueair_with_humiditycontent_and_enthalpy(parameters.humiditycontent,parameters.enthalpy,barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'selectionofaction_0'+str(i):selectionofaction,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            }
        elif (dotvalue==4 and selectionofaction==0):
            temperature = 0
            enthalpy = 0
            humiditycontent = request.POST['humiditycontent_0'+str(i)]
            relativities = request.POST['relativities_0'+str(i)]
            parameters=ProcessCalculation(temperature,enthalpy,humiditycontent,relativities)
            parameters.find_valueair_with_humiditycontent_and_relativities(parameters.humiditycontent,parameters.relativities,barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'selectionofaction_0'+str(i):selectionofaction,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            }
        elif (dotvalue==5 and selectionofaction==0):
            temperature = 0
            enthalpy = request.POST['enthalpy_0'+str(i)]
            humiditycontent = 0
            relativities = request.POST['relativities_0'+str(i)]
            parameters=ProcessCalculation(temperature,enthalpy,humiditycontent,relativities)
            parameters.find_valueair_with_enthalpy_and_relativities(parameters.enthalpy,parameters.relativities,barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'selectionofaction_0'+str(i):selectionofaction,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            }
        elif (processes==0 and selectionofaction==1):
            #Переменная j отвечает за элемент взятый из listvalue
            temperature=request.POST['lasttemperature_0'+str(i)]
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            parameters.find_valueair_with_process_hot(temperature,listvalue[j-1]['humiditycontent_0'+str(j)],barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):selectionofaction,
            'saturationtemperature_0'+str(i):parameters.saturationtemperature,
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            }
        elif (processes==5 and selectionofaction==1):
            #Переменная j отвечает за элемент взятый из listvalue
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            parameters.temperature=request.POST['lasttemperature_0'+str(i)]
            moisture=request.POST['moisture_0'+str(i)]
            heatinput=request.POST['heatinput_0'+str(i)]
            parameters.find_valueair_with_process_rayprocess_with_temperature(parameters.temperature,listvalue[j-1]['enthalpy_0'+str(j)],
            listvalue[j-1]['humiditycontent_0'+str(j)],moisture,heatinput,barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'airflowondew_0'+str(i):round(parameters.airflowondew),
            'airflowoncapacity_0'+str(i):round(parameters.airflowoncapacity),
            }
        elif (processes==6 and selectionofaction==1):
            #Переменная j отвечает за элемент взятый из listvalue
            parameters.humiditycontent=request.POST['lasthumiditycontent_0'+str(i)]
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            moisture=request.POST['moisture_0'+str(i)]
            heatinput=request.POST['heatinput_0'+str(i)]
            parameters.find_valueair_with_process_rayprocess_with_humiditycontent(parameters.humiditycontent,listvalue[j-1]['enthalpy_0'+str(j)],
            listvalue[j-1]['humiditycontent_0'+str(j)],moisture,heatinput,barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'airflowondew_0'+str(i):round(parameters.airflowondew),
            'airflowoncapacity_0'+str(i):round(parameters.airflowoncapacity)
            }
        elif (processes==7 and selectionofaction==1):
            parameters.enthalpy=request.POST['lastenthalpy_0'+str(i)]
            #Переменная j отвечает за элемент взятый из listvalue
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            moisture=request.POST['moisture_0'+str(i)]
            heatinput=request.POST['heatinput_0'+str(i)]
            parameters.find_valueair_with_process_rayprocess_with_enthalpy(parameters.enthalpy,listvalue[j-1]['enthalpy_0'+str(j)],
            listvalue[j-1]['humiditycontent_0'+str(j)],moisture,heatinput,barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'airflowondew_0'+str(i):round(parameters.airflowondew),
            'airflowoncapacity_0'+str(i):round(parameters.airflowoncapacity)
            }
        elif (processes==8 and selectionofaction==1):
            #Переменная j отвечает за элемент взятый из listvalue
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            capacity=float(request.POST['capacity_0'+str(i)])
            airmassflow=float(request.POST['airmassflow_0'+str(i)])
            parameters.find_valueair_with_process_hot_with_capacit(capacity,airmassflow,listvalue[j-1]['enthalpy_0'+str(j)],
            listvalue[j-1]['humiditycontent_0'+str(j)],barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            }
        #Адиабатное охлаждение
        elif (processes==3 and selectionofaction==1):
            temperature=request.POST['lasttemperature_0'+str(i)]
            #Переменная j равна той строке в таблице с которой будет взаимодействовать процесс
            airmassflow=request.POST['airmassflow_0'+str(i)]
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            parameters.find_valueair_with_process_adiabatic_cooling_with_temperature(temperature,listvalue[j-1]['enthalpy_0'+str(j)],barometricpressure,
            airmassflow,listvalue[j-1]['humiditycontent_0'+str(j)])
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'airmoisture_0'+str(i):parameters.airmoisture,
            }
        elif (processes==9 and selectionofaction==1):
            airmassflow=request.POST['airmassflow_0'+str(i)]
            humiditycontent=request.POST['lasthumiditycontent_0'+str(i)]
            #Переменная j равна той строке в таблице с которой будет взаимодействовать процесс
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            parameters.find_valueair_with_process_adiabatic_cooling_with_humiditycontent(humiditycontent,listvalue[j-1]['enthalpy_0'+str(j)],
            barometricpressure,airmassflow,listvalue[j-1]['humiditycontent_0'+str(j)])
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'airmoisture_0'+str(i):parameters.airmoisture,
            }
        elif (processes==10 and selectionofaction==1):
            airmassflow=request.POST['airmassflow_0'+str(i)]
            relativities=request.POST['lastrelativities_0'+str(i)]
            #Переменная j равна той строке в таблице с которой будет взаимодействовать процесс
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            parameters.find_valueair_with_process_adiabatic_cooling_with_relativities(relativities,listvalue[j-1]['enthalpy_0'+str(j)],
            barometricpressure,airmassflow,listvalue[j-1]['humiditycontent_0'+str(j)])
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'airmoisture_0'+str(i):parameters.airmoisture,
            }
        elif (processes==2 and selectionofaction==1):
            airmassflow=request.POST['airmassflow_0'+str(i)]
            airmoisture=request.POST['airmoisture_0'+str(i)]
            #Переменная j равна той строке в таблице с которой будет взаимодействовать процесс
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            parameters.find_valueair_with_process_isothermicalmoisture_with_airmoisture(airmoisture,listvalue[j-1]['temperature_0'+str(j)],
            barometricpressure,airmassflow,listvalue[j-1]['humiditycontent_0'+str(j)],listvalue[j-1]['enthalpy_0'+str(j)])
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'capacity_0'+str(i):parameters.capacity,
            }
        elif (processes==11 and selectionofaction==1):
            airmassflow=float(request.POST['airmassflow_0'+str(i)])
            relativities = float(request.POST['lastrelativities_0'+str(i)])
            #Переменная j равна той строке в таблице с которой будет взаимодействовать процесс
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            parameters.find_valueair_with_process_isothermicalmoisture_with_relativities(relativities,listvalue[j-1]['temperature_0'+str(j)],
            barometricpressure,airmassflow,listvalue[j-1]['enthalpy_0'+str(j)],listvalue[j-1]['humiditycontent_0'+str(j)])
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'capacity_0'+str(i):parameters.capacity,
            'airmoisture_0'+str(i):parameters.airmoisture,
            }
        elif (processes==12 and selectionofaction==1):
            airmassflow=float(request.POST['airmassflow_0'+str(i)])
            humiditycontent= float(request.POST['lasthumiditycontent_0'+str(i)])
            #Переменная j равна той строке в таблице с которой будет взаимодействовать процесс
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            parameters.find_valueair_with_process_isothermicalmoisture_with_humiditycontent(humiditycontent,listvalue[j-1]['temperature_0'+str(j)],
            barometricpressure,airmassflow,listvalue[j-1]['enthalpy_0'+str(j)],listvalue[j-1]['humiditycontent_0'+str(j)])
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'capacity_0'+str(i):parameters.capacity,
            'airmoisture_0'+str(i):parameters.airmoisture,
            }
        #Смешение 
        elif (processes==13 and selectionofaction==1):
            airmassflow=float(request.POST['airmassflow_0'+str(i)])
            airmassflowtwo=float(request.POST['airmassflowtwo_0'+str(i)])
            #Переменная j равна той строке в таблице с которой будет взаимодействовать процесс
            j=int(request.POST['selecttimenowproccess_0'+str(i)])
            dotval=int(request.POST['selecttimenowproccesstwo_0'+str(i)])
            parameters.find_valueair_with_process_mixing(airmassflow,airmassflowtwo,listvalue[j-1]['enthalpy_0'+str(j)],
            listvalue[dotval-1]['enthalpy_0'+str(dotval)],listvalue[j-1]['humiditycontent_0'+str(j)],
            listvalue[dotval-1]['humiditycontent_0'+str(dotval)],listvalue[j-1]['temperature_0'+str(j)],listvalue[dotval-1]['temperature_0'+str(dotval)],
            barometricpressure)
            some_data_to_dump = {
            'temperature_0'+str(i): round(parameters.temperature,2),
            'enthalpy_0'+str(i): round(parameters.enthalpy,2),
            'humiditycontent_0'+str(i): round(parameters.humiditycontent,2),
            'relativities_0'+str(i): round(parameters.relativities,2),
            'processes_0'+str(i):processes,
            'saturationtemperature_0'+str(i):round(parameters.saturationtemperature,2),
            'dewpoint_0'+str(i):round(parameters.dewpoint,2),
            'airdensity_0'+str(i):round(parameters.airdensity,2),
            'parcpressure_0'+str(i):round(parameters.parcpressure,2),
            'temperature_01'+str(i):parameters.temperature1,
            'temperature_02'+str(i):parameters.temperature2,
            'enthalpy_01'+str(i):parameters.enthalpy1,
            'enthalpy_02'+str(i):parameters.enthalpy2,
            'humiditycontent_01'+str(i):parameters.humiditycontent1,
            'humiditycontent_02'+str(i):parameters.humiditycontent2,
            'dotval_0'+str(i):dotval,
            }

        listvalue.append(some_data_to_dump)
        data = json.dumps(listvalue)
        i=i+1
    return HttpResponse(data, content_type='application/json')

def save_calculation(request):
    new_calculation = Calculation(
        master_id   = request.user.id,
        created     = timezone.now(),
        title       = request.POST['save_title'],
        barometricpressure = float(request.POST['barometricpressure'])
        )
    new_calculation.save()
    i=1
    listvalue=[]
    counter=float(request.POST['counter'])
    while (i<=counter):
        selectionofaction=float(request.POST['selectionofaction_0'+str(i)])
        barometricpressure=request.POST['barometricpressure']
        if (selectionofaction==1):
            processes=float(request.POST['processes_0'+str(i)])
        elif(selectionofaction==0):
            dotvalue=float(request.POST['dotvalue_0'+str(i)])

        #save date
        if (dotvalue==2 and selectionofaction==0):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = float(request.POST['temperature_0'+str(i)]),
                humidity            = 0,
                enthalpy            = 0,
                relativities        = float(request.POST['relativities_0'+str(i)]),
                selection_of_action = 0,
                type_of_action      = 2,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = 0,
                from_process2       = 0
            )

        elif (dotvalue==0 and selectionofaction==0):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = float(request.POST['temperature_0'+str(i)]),
                humidity            = 0,
                enthalpy            = float(request.POST['enthalpy_0'+str(i)]),
                relativities        = 0,
                selection_of_action = 0,
                type_of_action      = 0,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = 0,
                from_process2       = 0
            )

        elif (dotvalue==1 and selectionofaction==0):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = float(request.POST['temperature_0'+str(i)]),
                humidity            = float(request.POST['humiditycontent_0'+str(i)]),
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 0,
                type_of_action      = 1,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = 0,
                from_process2       = 0
            )
        elif (dotvalue==3 and selectionofaction==0):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = float(request.POST['humiditycontent_0'+str(i)]),
                enthalpy            = float(request.POST['enthalpy_0'+str(i)]),
                relativities        = 0,
                selection_of_action = 0,
                type_of_action      = 3,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = 0,
                from_process2       = 0
            )
        elif (dotvalue==4 and selectionofaction==0):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = float(request.POST['humiditycontent_0'+str(i)]),
                enthalpy            = 0,
                relativities        = float(request.POST['relativities_0'+str(i)]),
                selection_of_action = 0,
                type_of_action      = 4,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = 0,
                from_process2       = 0
            )

        elif (dotvalue==5 and selectionofaction==0):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = float(request.POST['enthalpy_0'+str(i)]),
                relativities        = float(request.POST['relativities_0'+str(i)]),
                selection_of_action = 0,
                type_of_action      = 5,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = 0,
                from_process2       = 0
            )
        elif (processes==0 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 0,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = float(request.POST['lasttemperature_0'+str(i)]),
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==5 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 5,
                heat_input          = float(request.POST['heatinput_0'+str(i)]),
                moisture            = float(request.POST['moisture_0'+str(i)]),
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = float(request.POST['lasttemperature_0'+str(i)]),
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==6 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 6,
                heat_input          = float(request.POST['heatinput_0'+str(i)]),
                moisture            = float(request.POST['moisture_0'+str(i)]),
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = float(request.POST['lasthumiditycontent_0'+str(i)]),
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==7 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 7,
                heat_input          = float(request.POST['heatinput_0'+str(i)]),
                moisture            = float(request.POST['moisture_0'+str(i)]),
                capacity            = 0,
                air_mass_flow       = 0,
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = float(request.POST['lastenthalpy_0'+str(i)]),
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==8 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 8,
                heat_input          = 0,
                moisture            = 0,
                capacity            = float(request.POST['capacity_0'+str(i)]),
                air_mass_flow       = float(request.POST['airmassflow_0'+str(i)]),
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        #Адиабатное охлаждение
        elif (processes==3 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 3,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = float(request.POST['airmassflow_0'+str(i)]),
                air_mass_flow2      = 0,
                final_temperature   = float(request.POST['lasttemperature_0'+str(i)]),
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==9 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 9,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = float(request.POST['airmassflow_0'+str(i)]),
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = float(request.POST['lasthumiditycontent_0'+str(i)]),
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==10 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 10,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = float(request.POST['airmassflow_0'+str(i)]),
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = float(request.POST['lastrelativities_0'+str(i)]),
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==2 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 2,
                heat_input          = 0,
                moisture            = float(request.POST['airmoisture_0'+str(i)]),
                capacity            = 0,
                air_mass_flow       = float(request.POST['airmassflow_0'+str(i)]),
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==11 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 11,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = float(request.POST['airmassflow_0'+str(i)]),
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = float(request.POST['lastrelativities_0'+str(i)]),
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==12 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 12,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = float(request.POST['airmassflow_0'+str(i)]),
                air_mass_flow2      = 0,
                final_temperature   = 0,
                final_humidity      = float(request.POST['lasthumiditycontent_0'+str(i)]),
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = 0
            )
        elif (processes==13 and selectionofaction==1):
            new_date_dot = DotNotCalculation(
                calculation         = new_calculation,
                temperature         = 0,
                humidity            = 0,
                enthalpy            = 0,
                relativities        = 0,
                selection_of_action = 1,
                type_of_action      = 13,
                heat_input          = 0,
                moisture            = 0,
                capacity            = 0,
                air_mass_flow       = float(request.POST['airmassflow_0'+str(i)]),
                air_mass_flow2      = float(request.POST['airmassflowtwo_0'+str(i)]),
                final_temperature   = 0,
                final_humidity      = 0,
                final_entalpy       = 0,
                final_relativities  = 0,
                from_process        = int(request.POST['selecttimenowproccess_0'+str(i)]),
                from_process2       = int(request.POST['selecttimenowproccesstwo_0'+str(i)])
            )



        new_date_dot.save()
        i=i+1

    return JsonResponse({'status' : 'success', 'message' : 'thats all'})

def old(request):
    if request.user.is_authenticated():
        calculation = Calculation.objects.filter(master_id = request.user.id)

        return render(request,'diagramm_old.jade',{'calculation':calculation})
    return HttpResponsePermanentRedirect("/enter/#login")


def old_calculation(request,calculation_id):
    if request.user.is_authenticated():
        dotnotnalculation = DotNotCalculation.objects.filter(calculation_id = calculation_id).order_by('id')
        counter = DotNotCalculation.objects.filter(calculation_id = calculation_id).count()
        calculation       = Calculation.objects.get(id=calculation_id)
        return render(request,'diagramm_old-item.jade',{'dotnotnalculation':dotnotnalculation,'calculation':calculation,'counter':counter})
    return HttpResponsePermanentRedirect("/enter/#login")


    

