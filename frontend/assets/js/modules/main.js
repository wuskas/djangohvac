$(document).ready(function() {

  $('[data-bg]').each(function() {
    $(this).backstretch($(this).data('bg'));
  });

});