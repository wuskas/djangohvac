$(document).ready(function() {

  $('.profile-block__form__content-avatar-row__buttom').on('click', function() {
    $('.profile-block-upload form input[type="file"]').trigger('click');
    console.log('start settings avatar');
  });

  $('.profile-block-upload').ajaxUpload({
    url : "/userprofile/uploadimg/",
    name: "avatar",
    onSubmit: function() {
    },
    onComplete: function(response) {
      console.log(response);
      response = JSON.parse(response);
      $('.profile-block__form input[name="avatar"]').val(response.avatar);
      $('.profile-block__form__content-avatar-row__img-src').attr('src', '/' + response.avatar + '?vr='+ Math.random());
    }
  });

  //city ajax for registration and profile settings

  $('.profile-block__form select[name="country"]').on('change', function() {
    var form = $(this).closest('form');
    var targetElement = $('.profile-block__form select[name="city"]')[0];
    var csrftoken = $.cookie('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
   
    $.ajax({
      url: '/all-cities/',
      type: 'POST',
      data: {
        country : $(this).val()
      },
      beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      },
      success: function(response) {
        if (response.status == 'success') {
          targetElement.options.length = 0;
          var data = JSON.parse(response.message);
          for (item in data) {
            var option = document.createElement('option');
            option.innerHTML = data[item].fields.title;
            option.value = data[item].pk;
            targetElement.appendChild(option);
          }
        }
      },
      error: function(response){
        console.log(response);
      }
    });
  });

  //profile-settings
  $('.profile-block__form__content-data__buttom-save').on('click', function() {

    var csrftoken = $.cookie('csrftoken');
    function csrfSafeMethod(method) {
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }


/*    $('.profile-block__form .has-error').removeClass('has-error');*/
    var form = $('.profile-block__form')[0];
    console.log(form)
    data = {
      firstName                : form.firstName.value,
      secondName               : form.secondName.value,
      city_id                  : form.city.value,
      country_id               : form.country.value,
    };

    //Validation
    var validationStatus = 'success';
    for (item in data) {
      if (data[item] == '') {
        $('[name=' + item + ']').parent().addClass('has-error');
        validationStatus = 'fail';
      }
    }

    if (validationStatus == 'fail') {
      return false;
    }


    $.ajax({
      url: '/userprofile/save/',
      type: 'POST',
      dataType: "json",
      data: $('.profile-block__form').serialize(),
      beforeSend: function() {
        $('.profile-block__form').parent().parent().addClass('processing');
        $('.has-error').removeClass('has-error');
      },
      complete: function() {
        $('.profile-block__form').parent().parent().removeClass('processing');
      },
      success: function(response) {
        if (response.status == 'success') {
          window.location.reload()
        }
      },
      error: function(response){
        console.log('client error');
        console.log(response);
      }
    });

  });


});