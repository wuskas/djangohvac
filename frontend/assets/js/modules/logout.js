$(document).ready(function() {
  $('.logout').on('click', function() {

    var csrftoken = $.cookie('csrftoken');

    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
   
    $.ajax({
      url: '/accounts/logout/',
      type: 'POST',
      beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      },
      success: function(response) {
        console.log(response);
        window.location.reload();
      },
      error: function(response){
        console.log(response);
      }
    });
    return false;
  });
});