$(document).ready(function() {


  $('.login-block-content-buttom__complete').on('click', function() {

    var csrftoken = $.cookie('csrftoken');
    
    function csrfSafeMethod(method) {
      // these HTTP methods do not require CSRF protection
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajax({
      url: '/accounts/login/',
      type: 'POST',
      dataType: "json",
      data: $('.login-block').serialize(),
      beforeSend: function(xhr, settings) {
        $('.login-block').parent().parent().addClass('processing');
        $('.login-block .error-messages').html('');
        $('.login-block .error-messages').removeClass('alert');
        $('.login-block .error-messages').removeClass('alert-danger');
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      },
      success: function(response) {
        console.log(response);
        window.location.reload();
      },
      error: function(response){
        $('.login-block').parent().parent().removeClass('processing');
        console.log(response);
        if(response.responseJSON.message == 'incorrect') {
          $('.login-block-error').addClass('alert');
          $('.login-block-error').addClass('alert-danger');
          $('.login-block-error').html('Неправильная пара логин-пароль');
        }
        if(response.responseJSON.message == 'user_disabled') {
          $('.login-block-error').addClass('alert');
          $('.login-block-error').addClass('alert-danger');
          $('.login-block-error').html('Вы не активировали учетную запись');
        }
      }
    });
    return false;
  });




});