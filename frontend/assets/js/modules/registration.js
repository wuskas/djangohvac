$(document).ready(function() {

  //registration
  $('.registration-block-content-buttom__complete').on('click', function() {


    var csrftoken = $.cookie('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    var form = $('.registration-block')[0];

    data = {
      username                 : form.username.value,
      email                    : form.email.value,
      firstName                : form.firstName.value,
      secondName               : form.secondName.value,
      password1                : form.password1.value,
      password2                : form.password2.value
    };

    //Validation
    var validationStatus = 'success';
    for (item in data) {
      if (data[item] == '') {
        $('[name=' + item + ']').parent().addClass('has-error');
        validationStatus = 'fail';
      }
    }

    if (validationStatus == 'fail') {
      return false;
    }

    if (form.password1.value != form.password2.value){
      form.password1.parentNode.className = form.password2.parentNode.className +' has-error';
      form.password2.parentNode.className = form.password2.parentNode.className +' has-error';
      return false;
    }


    $.ajax({
      url: '/accounts/register/',
      type: 'POST',
      dataType: "json",
      data: $('.registration-block').serialize(),
      beforeSend: function(xhr, settings) {
        $('.registration-block').parent().addClass('processing');
        $('.has-error').removeClass('has-error');
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      },
/*      function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      },*/
      complete: function() {
        $('.container').removeClass('processing');
      },
      success: function(response) {
        $('.registration-block').remove();
        $('.registration-block-message').html(response.message);
      },
      error: function(response){
        console.log(response);
        if (response.responseJSON.username) {
          $('input[name="username"]').parent().addClass('has-error');
        }        
        if (response.responseJSON.email) {
          $('input[name="email"]').parent().addClass('has-error');
        }
      }
    });

  });
 
});